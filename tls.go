package deskwapp

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/gofrs/uuid"
)

type CertConfig struct {
	CommonName string
	Valid      time.Duration
}

type TLSServer struct {
	Addr     string
	CertFile string
	KeyFile  string
	KeyGen   KeyGenerator
}

func (s *TLSServer) HasCert() bool {
	if _, err := os.Stat(s.CertFile); os.IsNotExist(err) {
		return false
	}
	if _, err := os.Stat(s.KeyFile); os.IsNotExist(err) {
		return false
	}
	return true
}

func (s *TLSServer) NewCert(passphrase []byte, cfg *CertConfig) (err error) {
	pKey, err := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	if err != nil {
		return err
	}
	validStart := time.Now()
	var validTil time.Time
	if cfg.Valid > 0 {
		validTil = validStart.Add(cfg.Valid)
	}
	serNo, err := uuid.NewV4()
	if err != nil {
		return err
	}
	cerTmpl := x509.Certificate{
		SerialNumber:          new(big.Int).SetBytes(serNo.Bytes()),
		Subject:               pkix.Name{CommonName: cfg.CommonName},
		NotBefore:             validStart,
		NotAfter:              validTil,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth}, // x509.ExtKeyUsageClientAuth},
		BasicConstraintsValid: true,
	}
	cerDer, err := x509.CreateCertificate(
		rand.Reader,
		&cerTmpl, &cerTmpl,
		pKey.Public(), pKey)
	if err != nil {
		return fmt.Errorf("create cert: %w", err)
	}

	wr, err := os.Create(s.CertFile)
	if err != nil {
		return fmt.Errorf("create cert-file '%s': %w", s.CertFile, err)
	}
	defer wr.Close()
	err = pem.Encode(wr, &pem.Block{Type: "CERTIFICATE", Bytes: cerDer})
	if err != nil {
		return fmt.Errorf("write cert to '%s': %w", s.CertFile, err)
	}
	err = wr.Close()
	if err != nil {
		return fmt.Errorf("close cert-file '%s': %w", s.CertFile, err)
	}

	ecpem, err := x509.MarshalECPrivateKey(pKey)
	if err != nil {
		return fmt.Errorf("marshal private key: %w", err)
	}
	block := &pem.Block{Type: "EC PRIVATE KEY", Bytes: ecpem}
	err = CryptWriteFile(s.KeyFile, s.KeyGen, passphrase, pem.EncodeToMemory(block))
	if err != nil {
		err = fmt.Errorf("write key-file '%s': %w", s.KeyFile, err)
	}
	return err
}

// Inspred by https://gist.github.com/tjamet/c9a53127c9bec54f62ed94685de85875
func (s *TLSServer) ListenAndServe(passphrase []byte, handler http.Handler) error {
	certPEMBlock, err := os.ReadFile(s.CertFile)
	if err != nil {
		return err
	}
	keyPEMBlock, err := CryptReadFile(s.KeyFile, s.KeyGen, passphrase)
	if err != nil {
		return fmt.Errorf("read %s: %w", s.KeyFile, err)
	}
	cert, err := tls.X509KeyPair(certPEMBlock, keyPEMBlock)
	if err != nil {
		return err
	}
	ln, err := net.Listen("tcp", s.Addr)
	if err != nil {
		return err
	}
	server := &http.Server{
		Addr:    s.Addr,
		Handler: handler,
		TLSConfig: &tls.Config{
			Certificates: []tls.Certificate{cert},
		},
	}
	return server.ServeTLS(ln, "", "")
}
