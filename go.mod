module git.fractalqb.de/fractalqb/deskwapp

go 1.16

require (
	github.com/gofrs/uuid v4.0.0+incompatible
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
)
