# deskwapp

Helpful utilities to create web applications that run on your desktop
machine to be accessed remotely by one user—most likely yourself!

Initial ideas were inspired by
[Syncthing](https://syncthing.net/downloads/) which is an incredibly
useful tool. That inspiration was needed for
[BC+](https://github.com/CmdrVasquess/bcplus)—my Go-learning project
back from the days when I liked to play ED very much. Then, when I
published [Gamcro](https://github.com/CmdrVasquess/gamcro) to help out
some people searching for something like that, I realized that a more
in depth look at security was necessary. This Go module encapsulates
what I learned from those efforts.
