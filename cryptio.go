package deskwapp

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
	"os"
)

const (
	cryptIOVersion = 1 // FIXME shall depend on KeyGenerator
	cryptSaltSize  = 16
)

func CryptWriteFile(name string, g KeyGenerator, passwd, data []byte) error {
	wr, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	defer wr.Close()
	return CryptWrite(wr, g, passwd, data)
}

func CryptWrite(wr io.Writer, g KeyGenerator, passwd, data []byte) error {
	if len(passwd) == 0 {
		if _, err := wr.Write([]byte{0}); err != nil {
			return err
		}
		_, err := wr.Write(data)
		return err
	}
	key, salt, err := g.Generate(passwd, nil)
	if err != nil {
		return err
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return err
	}
	nonce := make([]byte, aesgcm.NonceSize())
	if _, err = rand.Read(nonce); err != nil {
		return err
	}
	ciph := aesgcm.Seal(nil, nonce, data, nil)
	if _, err := wr.Write([]byte{cryptIOVersion}); err != nil {
		return err
	}
	if _, err = wr.Write(salt); err != nil {
		return err
	}
	if _, err = wr.Write(nonce); err != nil {
		return err
	}
	if _, err = wr.Write(ciph); err != nil {
		return err
	}
	return nil
}

func CryptReadFile(name string, g KeyGenerator, passwd []byte) ([]byte, error) {
	rd, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer rd.Close()
	return CryptRead(rd, g, passwd)
}

func CryptRead(rd io.Reader, g KeyGenerator, passwd []byte) ([]byte, error) {
	if len(passwd) == 0 {
		var buf bytes.Buffer
		if _, err := io.CopyN(&buf, rd, 1); err != nil {
			return nil, err
		}
		if buf.Bytes()[0] != 0 {
			return nil, fmt.Errorf(
				"detected crypt IO version %d for cleartext read",
				buf.Bytes()[0],
			)
		}
		buf.Reset()
		if _, err := io.Copy(&buf, rd); err != nil {
			return nil, err
		}
		return buf.Bytes(), nil
	}
	var salt bytes.Buffer
	if _, err := io.CopyN(&salt, rd, 1); err != nil {
		return nil, err
	}
	if salt.Bytes()[0] != cryptIOVersion {
		return nil, fmt.Errorf(
			"detected crypt IO version %d instead of %d",
			salt.Bytes()[0],
			cryptIOVersion,
		)
	}
	salt.Reset()
	if _, err := io.CopyN(&salt, rd, cryptSaltSize); err != nil {
		return nil, err
	}
	key, _, _ := g.Generate(passwd, salt.Bytes())
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	var nonce bytes.Buffer
	if _, err := io.CopyN(&nonce, rd, int64(aesgcm.NonceSize())); err != nil {
		return nil, err
	}
	var ciph bytes.Buffer
	if _, err := io.Copy(&ciph, rd); err != nil {
		return nil, err
	}
	plaintext, err := aesgcm.Open(nil, nonce.Bytes(), ciph.Bytes(), nil)
	if err != nil {
		return nil, err
	}
	return plaintext, nil
}
