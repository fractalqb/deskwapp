package deskwapp

import (
	"bufio"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"os"
)

func WriteAuthCredsFile(name string, ac *AuthCreds) error {
	wr, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	defer wr.Close()
	return WriteAuthCreds(wr, ac)
}

func WriteAuthCreds(wr io.Writer, ac *AuthCreds) (err error) {
	if _, err = fmt.Fprintln(wr, ac.user); err != nil {
		return err
	}
	_, err = fmt.Fprintln(wr, base64.StdEncoding.EncodeToString(ac.salt))
	if err != nil {
		return err
	}
	_, err = fmt.Fprintln(wr, base64.StdEncoding.EncodeToString(ac.passKey))
	return err
}

func ReadAuthCredsFile(name string, ac *AuthCreds) error {
	rd, err := os.Open(name)
	if err != nil {
		return err
	}
	defer rd.Close()
	return ReadAuthCreds(rd, ac)
}

func ReadAuthCreds(rd io.Reader, ac *AuthCreds) (err error) {
	scan := bufio.NewScanner(rd)
	switch {
	case !scan.Scan():
		return errors.New("no user name in auth data")
	case scan.Err() != nil:
		return scan.Err()
	}
	ac.user = scan.Text()
	switch {
	case !scan.Scan():
		return errors.New("no salt in auth data")
	case scan.Err() != nil:
		return scan.Err()
	}
	ac.salt, err = base64.StdEncoding.DecodeString(scan.Text())
	if err != nil {
		return err
	}
	switch {
	case !scan.Scan():
		return errors.New("no password hash in auth data")
	case scan.Err() != nil:
		return scan.Err()
	}
	ac.passKey, err = base64.StdEncoding.DecodeString(scan.Text())
	return err
}
