package deskwapp

import (
	"fmt"
)

func ExampleStringCast() {
	s := "foobarbaz"
	b := []byte(s)
	for i := range b {
		b[i] = 0
	}
	fmt.Println(s)
	fmt.Println(b)
	// Output:
	// foobarbaz
	// [0 0 0 0 0 0 0 0 0]
}
