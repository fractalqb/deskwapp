package deskwapp

import (
	"bytes"
	"crypto/rand"
	"crypto/subtle"
	"fmt"
	"math/big"
	mrand "math/rand"
	"net/http"
	"strings"
	"time"

	"golang.org/x/crypto/argon2"
)

type KeyGenerator interface {
	// When salt is not nil, Generate takes passwd and salt and derives a key
	// from them returning also the given salt. If salt is nil, Generate first
	// creates a random salt and then derives the key returing also the new
	// salt.
	Generate(passwd, salt []byte) (key, nsalt []byte, err error)
}

type Argon2idKeyGen struct {
	SaltSize    int
	KDFTime     uint32 // argon2.IDKey: 1
	KDFMemory   uint32 // argon2.IDKey: 64*1024
	KDFParallel uint8  // argon2.IDKey: 4
	KDFKeyLen   uint32
}

func (a2k *Argon2idKeyGen) Generate(passwd, salt []byte) (key, nsalt []byte, err error) {
	if salt == nil {
		salt = make([]byte, a2k.SaltSize)
		if _, err = rand.Read(salt); err != nil {
			return nil, nil, err
		}
	}
	key = argon2.IDKey(
		passwd,
		salt,
		a2k.KDFTime,
		a2k.KDFMemory,
		a2k.KDFParallel,
		a2k.KDFKeyLen,
	)
	return key, salt, nil
}

type AuthCreds struct {
	keyGen    KeyGenerator
	user      string
	salt      []byte
	passKey   []byte
	passCache []byte
}

func NewAuthCreds(keyGen KeyGenerator) *AuthCreds {
	return &AuthCreds{keyGen: keyGen}
}

func (ac *AuthCreds) Set(user string, passwd []byte) (err error) {
	key, salt, err := ac.keyGen.Generate(passwd, nil)
	if err != nil {
		return err
	}
	ac.user = user
	ac.salt = salt
	ac.passKey = key
	ac.passCache = nil
	return nil
}

func (ac *AuthCreds) SetString(user, passwd string) (err error) {
	p := []byte(passwd)
	res := ac.Set(user, p)
	for i := range p {
		p[i] = 0
	}
	return res
}

func (ac *AuthCreds) Check(user string, passwd []byte) bool {
	if ac.user != user {
		return false
	}
	if ac.passCache != nil {
		return subtle.ConstantTimeCompare(passwd, ac.passCache) == 1
	}
	h, _, _ := ac.keyGen.Generate([]byte(passwd), ac.salt)
	res := subtle.ConstantTimeCompare(h, ac.passKey) == 1
	if res {
		ac.passCache = bytes.Repeat(passwd, 1)
	}
	return res
}

func (ac *AuthCreds) CheckString(user, passwd string) bool {
	p := []byte(passwd)
	res := ac.Check(user, p)
	for i := range p {
		p[i] = 0
	}
	return res
}

type BasicAuth struct {
	Realm        string
	Creds        *AuthCreds `json:"-"`
	FailMinDelay time.Duration
	FailMaxDelay time.Duration

	FailMsg    func(user string, rq *http.Request)
	SuccessMsg func(user string, rq *http.Request)
}

func (ba *BasicAuth) SetRealm(from string) error {
	key, err := makeRandStr(realmChars, 6)
	if err != nil {
		return err
	}
	ba.Realm = fmt.Sprintf(from, key)
	return nil
}

const realmChars = "0123456789ABCDEFGHJKLMNPQRTUVW"

func makeRandStr(chars string, strlen int) (string, error) {
	charNo := big.NewInt(int64(len(chars)))
	var sb strings.Builder
	for strlen > 0 {
		c, err := rand.Int(rand.Reader, charNo)
		if err != nil {
			return "", err
		}
		sb.WriteByte(chars[c.Uint64()])
		strlen--
	}
	return sb.String(), nil
}

func (ba *BasicAuth) Protect(h http.HandlerFunc) http.HandlerFunc {
	return func(wr http.ResponseWriter, rq *http.Request) {
		user, pass, ok := rq.BasicAuth()
		if !ok {
			auth := fmt.Sprintf(`Basic realm="%s"`, ba.Realm)
			wr.Header().Set("WWW-Authenticate", auth)
			http.Error(wr, "Unauthorized", http.StatusUnauthorized)
			return
		} else if !ba.Creds.CheckString(user, pass) {
			if ba.FailMsg != nil {
				ba.FailMsg(user, rq)
			}
			var delay time.Duration
			if ba.FailMaxDelay <= ba.FailMinDelay {
				delay = ba.FailMinDelay
			} else {
				min := int64(ba.FailMinDelay)
				max := int64(ba.FailMaxDelay)
				delay = time.Duration(min + mrand.Int63n(max-min))
			}
			time.Sleep(delay)
			http.Error(wr, "Forbidden", http.StatusForbidden)
			return
		} else if ba.SuccessMsg != nil {
			ba.SuccessMsg(user, rq)
		}
		h(wr, rq)
	}
}
